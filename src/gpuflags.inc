NVFLAGS += -D__WITH_GPU
NVFLAGS += -D__WITH_UM
#NVFLAGS += -D__WITH_NVTX
#NVFLAGS += -g -G  
NVFLAGS += -g -O3 
#NVFLAGS += -fPIC
NVFLAGS += -use_fast_math
NVFLAGS += -std=c++11
