#ifndef WRAPPER_BAYESYS_H
#define WRAPPER_BAYESYS_H

// Include
#include <iostream>
//#include "module_readParameters.hpp"
#include "module_cosmodistances.hpp"
#include <cstring>
#include <sstream>
#include <stdlib.h>

#include <structure_hpc.hpp>
#include "bayesys3.h"
#include "random.h"
#include "userstr.h"
#include <allocation_GPU.cuh>
#include <allocation.hpp>
#include <module_readParameters.hpp>

int
runBayeSys3 (void
(*chi2_function) (double*, int*, struct runmode_param *, const struct Potential_SOA*, const struct grid_param *, const int*, galaxy*),
	     char* FOLDER, runmode_param *runmode, grid_param *frame, Potential_SOA* potentials, potentialoptimization *potentialsToOptimize, galaxy* images,
	     int * nImagesSet, double BayeSys3Rate, int useGPU);

int BayeSys3_getNumberDim (int &numberDim, potentialoptimization potentialsToOptimize[], int nhalos);
int BayeSys3_calculateChi2 (double &chi2, CommonStr* Common);
int BayeSys3_rescaleCube_1Atom (double *cube, CommonStr* Common);
int BayeSys3_rescaleParam (int haloID, std::string parameterID, double *parameterValue, CommonStr* Common);
double BayeSys3_prior (CommonStr* Common, double parameterValue, double min, double max);
double BayeSys3_uniformPrior (double val, double min, double max);
double BayeSys3_calculateLogLikelihood (double &chi2, int &error, CommonStr* Common);
int BayeSys3_setReferencePotential (CommonStr* Common);
int module_BayeSys3_addStatistics (double value, UserCommonStr *UserCommon, int param, long int totalNumberSamples);
int BayeSys3_returnBestPotential (Potential_SOA * potentials, CommonStr* Common);


#endif
