/**
Lenstool-HPC: HPC based massmodeling software and Lens-map generation
Copyright (C) 2017  Christoph Schaefer, EPFL (christophernstrerne.schaefer@epfl.ch), Gilles Fourestey (gilles.fourestey@epfl.ch)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

@brief: Function for first order derivative computation over a grid

*/


#include "grid_gradient_CPU.hpp"
//
#ifdef __WITH_MPI
#include<mpi.h>
#endif
//
//
//
void gradient_grid_general_CPU(type_t *grid_grad_x, type_t *grid_grad_y, const struct grid_param *frame, const struct Potential_SOA *lens, int Nlens, type_t dx, type_t dy, int nbgridcells_x, int nbgridcells_y, int istart, int jstart);

void gradient_grid_CPU(type_t *grid_grad_x, type_t *grid_grad_y, const struct grid_param *frame, const struct Potential_SOA *lens, int nhalos, int nbgridcells, int istart, int jstart)
{
	type_t dx = (frame->xmax - frame->xmin)/(nbgridcells - 1);
	type_t dy = (frame->ymax - frame->ymin)/(nbgridcells - 1);
	//
	gradient_grid_general_CPU(grid_grad_x, grid_grad_y, frame, lens, nhalos, dx, dy, nbgridcells, nbgridcells, 0, 0);
}

void gradient_grid_CPU(type_t *grid_grad_x, type_t *grid_grad_y, const struct grid_param *frame, const struct Potential_SOA *lens, int nhalos, int nbgridcells_x, int nbgridcells_y, int istart, int jstart)
{
	type_t dx = (frame->xmax - frame->xmin)/(nbgridcells_x - 1);
	type_t dy = (frame->ymax - frame->ymin)/(nbgridcells_y - 1);
	//
	printf("istart = %d, jstart = %d, nbgridcells_x = %d, ngridcells_y = %d\n", istart, jstart, nbgridcells_x, nbgridcells_y);
	gradient_grid_general_CPU(grid_grad_x, grid_grad_y, frame, lens, nhalos, dx, dy, nbgridcells_x, nbgridcells_y, istart, jstart);
}
//
//
//
void gradient_grid_CPU(type_t *grid_grad_x, type_t *grid_grad_y, const struct grid_param *frame, const struct Potential_SOA *lens, int nhalos, type_t dx, type_t dy, int nbgridcells_x, int nbgridcells_y, int istart, int jstart)
{
	gradient_grid_general_CPU(grid_grad_x, grid_grad_y, frame, lens, nhalos, dx, dy, nbgridcells_x, nbgridcells_y, istart, jstart);
}
//
//void gradient_grid_CPU_AOS(type_t *grid_grad_x, type_t *grid_grad_y, const struct grid_param *frame, const struct Potential_SOA *lens, int nhalos, type_t dx, type_t dy, int nbgridcells_x, int nbgridcells_y, int istart, int jstart)
//{
//        gradient_grid_general_CPU(grid_grad_x, grid_grad_y, frame, lens, nhalos, dx, dy, nbgridcells_x, nbgridcells_y, istart, jstart);
//}
//
//
//
void gradient_grid_general_CPU(type_t *grid_grad_x, type_t *grid_grad_y, const struct grid_param *frame, const struct Potential_SOA *lens, int Nlens, type_t dx, type_t dy, int nbgridcells_x, int nbgridcells_y, int istart, int jstart)
{
	int bid = 0; // index of the block (and of the set of images)
	int tid = 0; // index of the thread within the block
	//
	type_t /*dx, dy,*/ x_pos, y_pos;        //pixelsize
	int    grid_dim;
	point  Grad, image_point, true_coord_rotation;
	//
	grid_dim = nbgridcells_x*nbgridcells_y;
	//
#pragma omp parallel for 
	for (int jj = 0; jj < nbgridcells_y; ++jj)
		for (int ii = 0; ii < nbgridcells_x; ++ii)
		{
			//  (index < grid_dim*grid_dim)

			int index = jj*nbgridcells_x + ii;
			//grid_grad_x[index] = 0.;
			//grid_grad_y[index] = 0.;
			//
			struct point image_point;
			image_point.x = frame->xmin + (ii + istart)*dx;
			image_point.y = frame->ymin + (jj + jstart)*dy;
			//
			struct point Grad = module_potentialDerivatives_totalGradient_SOA(&image_point, lens, Nlens);
			//
			grid_grad_x[index] = Grad.x;
			grid_grad_y[index] = Grad.y;
			//
		}
	//std::cout << std::endl;
}


void gradient_grid_general_CPU_AOS(type_t *grid_grad_x, type_t *grid_grad_y, const struct grid_param *frame, const struct Potential *lens, int Nlens, type_t dx, type_t dy, int nbgridcells_x, int nbgridcells_y, int istart, int jstart)
{
        int bid = 0; // index of the block (and of the set of images)
        int tid = 0; // index of the thread within the block
        //
        type_t /*dx, dy,*/ x_pos, y_pos;        //pixelsize
        int    grid_dim;
        point  Grad, image_point, true_coord_rotation;
        //
        grid_dim = nbgridcells_x*nbgridcells_y;
        //
#pragma omp parallel for 
        for (int jj = 0; jj < nbgridcells_y; ++jj)
                for (int ii = 0; ii < nbgridcells_x; ++ii)
                {
                        //  (index < grid_dim*grid_dim)

                        int index = jj*nbgridcells_x + ii;
                        //grid_grad_x[index] = 0.;
                        //grid_grad_y[index] = 0.;
                        //
                        struct point image_point;
                        image_point.x = frame->xmin + (ii + istart)*dx;
                        image_point.y = frame->ymin + (jj + jstart)*dy;
                        //
                        struct point Grad = module_potentialDerivatives_totalGradient(&image_point, lens, Nlens);
                        //
                        grid_grad_x[index] = Grad.x;
                        grid_grad_y[index] = Grad.y;
                        //
                }
        //std::cout << std::endl;
}

void gradient_grid_CPU_AOS(type_t *grid_grad_x, type_t *grid_grad_y, const struct grid_param *frame, const struct Potential *lens, int nhalos, int nbgridcells, int istart, int jstart)
{
        type_t dx = (frame->xmax - frame->xmin)/(nbgridcells - 1);
        type_t dy = (frame->ymax - frame->ymin)/(nbgridcells - 1);
        //
        gradient_grid_general_CPU_AOS(grid_grad_x, grid_grad_y, frame, lens, nhalos, dx, dy, nbgridcells, nbgridcells, 0, 0);
}

