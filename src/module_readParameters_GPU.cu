#include "grid_gradient_GPU.cuh"
#include "gradient.hpp"
#include "gradient_GPU.cuh"
#include "gradient.hpp"

#include "module_readParameters.hpp"

extern void cudasafe( cudaError_t error, const char* message);

void checkCudaError(cudaError_t error)
{
  if(error != cudaSuccess) printf("Error: %s\n", cudaGetErrorString(error));
}
//
//
//
void module_readParameters_PotentialSOA_noalloc_GPU(std::string infile, Potential_SOA *lens_SOA, int nhalos, int n_tot_halos, cosmo_param cosmology)
{
	//module_readParameters_PotentialSOA_noalloc(infile, lens_SOA, nhalos, n_tot_halos, cosmology);
#if defined(__WITH_UM)
    #if (__CUDA_ARCH__ >= 600)
// let's prefetch the data in the GPU memory
#warning "Prefetching data on GPU"
	    checkCudaError(cudaMemPrefetchAsync(lens_SOA 				, sizeof(Potential_SOA), 0));
        checkCudaError(cudaMemPrefetchAsync(lens_SOA->position_x		, nhalos*sizeof(type_t), 0));
        checkCudaError(cudaMemPrefetchAsync(lens_SOA->position_y		, nhalos*sizeof(type_t), 0));
        checkCudaError(cudaMemPrefetchAsync(lens_SOA->anglecos			, nhalos*sizeof(type_t), 0));
        checkCudaError(cudaMemPrefetchAsync(lens_SOA->anglesin			, nhalos*sizeof(type_t), 0));
        checkCudaError(cudaMemPrefetchAsync(lens_SOA->ellipticity_potential	, nhalos*sizeof(type_t), 0));
        checkCudaError(cudaMemPrefetchAsync(lens_SOA->rcore			, nhalos*sizeof(type_t), 0));
        checkCudaError(cudaMemPrefetchAsync(lens_SOA->rcut			, nhalos*sizeof(type_t), 0));
        checkCudaError(cudaMemPrefetchAsync(lens_SOA->b0			, nhalos*sizeof(type_t), 0));
        checkCudaError(cudaMemPrefetchAsync(lens_SOA->ellipticity_angle		, nhalos*sizeof(type_t), 0));
        checkCudaError(cudaMemPrefetchAsync(lens_SOA->ellipticity		, nhalos*sizeof(type_t), 0));
        checkCudaError(cudaMemPrefetchAsync(lens_SOA->vdisp			, nhalos*sizeof(type_t), 0));
        checkCudaError(cudaMemPrefetchAsync(lens_SOA->z				, nhalos*sizeof(type_t), 0));
        checkCudaError(cudaMemPrefetchAsync(lens_SOA->N_types			, nhalos*sizeof(int), 0));
        checkCudaError(cudaMemPrefetchAsync(lens_SOA->type			, nhalos*sizeof(int), 0));
    #endif
#endif

}

