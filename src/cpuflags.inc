CXXFLAGS += -fPIC -qopenmp -pedantic -lcfitsio
CXXFLAGS += -std=c++0x
CXXFLAGS += -ftz -g
#CXXFLAGS += -no-vec
#CXXFLAGS += -xAVX2 
