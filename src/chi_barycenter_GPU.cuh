#pragma once
#ifndef __CHI_GPU_HPP__
#define __CHI_GPU_HPP__

#include <structure_hpc.hpp>
#include <grid_srcplane_conversion.hpp>
#include <grid_gradient_CPU.hpp>
#include <gradient.hpp>
//#include <gradient_avx.hpp>
//#ifdef __AVX512F__
//#include "gradient_avx512f.hpp"
//#endif

void mychi_bruteforce_SOA_GPU_grid_gradient_barycentersource(type_t* grid_gradient_x, type_t* grid_gradient_y, double *chi, int *error, runmode_param *runmode, const struct Potential_SOA *lens, const struct grid_param *frame, const int *nimages_strongLensing, galaxy *images, double dx, double dy, int y_pos_loc, int y_bound);
//void mychi_bruteforce_SOA_GPU_grid_gradient_barycentersource(double *chi, int *error, runmode_param *runmode, const struct Potential_SOA *lens, const struct grid_param *frame, const int *nimages_strongLensing, galaxy *images);

#endif
