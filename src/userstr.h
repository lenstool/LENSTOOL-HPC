/**
 * @file   userstr.h
 * @Author Markus Rexroth
 * @date   July 2015
 * @version 0,1
 * @brief  Define user structures. Bayesian Inference / Massive Inference
 */
#ifndef USERSTRH
#define USERSTRH

/** @brief contains all gpu parameters
 *
 * @param chi2_gpu: chi2 variable on the gpu
 * @param images_gpu: array of images on gpu, galaxy structure contain position, shape and cosmodistance information
 * @param nImagesSet_gpu: array of number of images per set on the gpu
 * @param runmode_gpu: runmode information
 * @param lens_gpu: array of potential parameters on gpu
 * @param frame_gpu: frame information
 * @param error_gpu: Error on the gpu
 */

typedef struct
{
  double *chi2_gpu;      		// chi2 variable on the gpu
  galaxy *images_gpu;			// array of images on gpu, galaxy structure contain position, shape and cosmodistance information
  int *nImagesSet_gpu;  		// array of number of images per set on the gpu
  runmode_param *runmode_gpu;	// runmode information
  Potential *lens_gpu;  	// array of potential parameters on gpu
  grid_param *frame_gpu;		// frame information
  int *error_gpu;       		// Error on the gpu
} BayeSys3_gpuStruct;

// UserCommonStr provided by JS and modified by Markus Rexroth
/** @brief structure provided for the bayesis3 code to add user specific information
 *
 * @param Nsample: O # output ensembles
 * @param atoms: O <# atoms>
 * @param Nchi2: # of calls to o_chi()
 * @param err: cumulated sum(x^2) for each parameter
 * @param avg: cumulated sum(x) for each parameter
 * @param sum: cumulated sum(var/mean/mean) for all parameters
 * @param nsets: Number of sets of multiple images
 * @param nhalos: Number of halos
 * @param potentialsToOptimize: Parameters to optimize for each halo
 * @param potentials: Potential parameters for each halo
 * @param reference_potential: Reference potential for lowest chi2
 * @param referenceTime: Timer
 * @param nBlocks_gpu: number of blocks on the GPU
 * @param use_gpu: Whether we use the gpu or not (1 is yes)
 * @param chi2_function: function pointer to chi2_function
 * @param gpuStruct: Struct with data that will be copied to the GPU
 * @param multipleimagesmin:  Multiple image area lower left corner
 * @param multipleimagesmax: Multiple image area upper right corner
 * @param pixel_size: Pixel size of coordinate system in image plane
 * @param narclets: Number of arclets
 * @param lowest_chi2: lowest chi2
 * @param outputPath: Path for output files
 */
typedef struct          // COMMON PARAMETERS AND STATISTICS
{
  long int Nsample;    //   O # output ensembles
  double atoms;      //   O <# atoms>
  long int Nchi2;	    // # of calls to o_chi()
  double *err;       // cumulated sum(x^2) for each parameter
  double *avg;       // cumulated sum(x) for each parameter
  double sum;        // cumulated sum(var/mean/mean) for all parameters
  int nsets;      // Number of sets of multiple images
  int nhalos;     // Number of strong halos
  int n_tot_halos;     // Number of all halos
  int *nImagesSet;	//Number of images per set
  potentialoptimization *potentialsToOptimize;  // Parameters to optimize for each halo
  Potential_SOA *potentials;  // Potential parameters for each halo
  Potential_SOA *reference_potential;  // Reference potential for lowest chi2
  runmode_param *runmode;
  grid_param *frame;
  galaxy * images;


  clock_t referenceTime;  // Timer
  int nBlocks_gpu;  // number of blocks on the GPU
  int use_gpu;      // Whether we use the gpu or not (1 is yes)
  void
  (*chi2_function) (double*, int*, struct runmode_param *, const struct Potential_SOA*, const struct grid_param *, const int*, galaxy*); // function pointer to chi2_function
  void *gpuStruct;  // Struct with data that will be copied to the GPU
  point multipleimagesmin;  // Multiple image area lower left corner
  point multipleimagesmax;  // Multiple image area upper right corner
  point pixel_size;     // Pixel size of coordinate system in image plane
  int narclets;     // Number of arclets
  double lowest_chi2;   // lowest chi2
  std::string outputPath; // Path for output files
} UserCommonStr;

/*
 typedef struct          // INDIVIDUAL PARAMETERS
 {
 .....
 } UserObjectStr;
 */

#endif
