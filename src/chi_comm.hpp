#include <string.h>
#include "structure_hpc.hpp"
#ifdef __WITH_MPI
#include <mpi.h>
#include "mpi_check.h"
#endif
//
void
delense_comm(int* numimagesfound, int MAXIMPERSOURCE, struct point* imagesposition, int* numimg, const int *nimages_strongLensing, const int* locimagesfound, struct point* image_pos, const int nsets, const int world_rank, const int world_size);
