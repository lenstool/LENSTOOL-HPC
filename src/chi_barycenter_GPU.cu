#include <iostream>
#include <string.h>
//#include <cuda_runtime.h>
#include <math.h>
#include <sys/time.h>
#include <fstream>
#include <unistd.h>
#include <assert.h>


#ifndef __xlC__
#warning "gnu compilers"
#endif

//#include "simd_math.h"
#include "chi_CPU.hpp"
//#ifdef __WITH_GPU
#include "grid_gradient_GPU.cuh"
////#endif
//#include "gradient_GPU.cuh"
#ifdef _OPENMP
#include <omp.h>
#endif
#ifdef __WITH_MPI
#warning "MPI enabled"
#include <mpi.h>
#include "mpi_check.h"
#include "chi_comm.hpp"
#endif

#include "delense_CPU_utils.hpp"
#include "delense_CPU.hpp"
#include "delense_GPU.cuh"
#include "chi_comm.hpp"
#include "chi_computation.hpp"

#ifdef __WITH_GPU
#include <cuda_runtime.h>
#include "nvml_GPU.cuh"
#include "cudafunctions.cuh"
#endif

#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))

int MAXIMPERSOURCE;
//
//
//
double myseconds()
{
	struct timeval  tp;
	struct timezone tzp;
	int i;

	i = gettimeofday(&tp,&tzp);
	return ( (double) tp.tv_sec + (double) tp.tv_usec * 1.e-6 );
}

void mychi_bruteforce_SOA_GPU_grid_gradient_barycentersource(type_t* grid_gradient_x, type_t* grid_gradient_y, double *chi, int *error, runmode_param *runmode, const  struct Potential_SOA *lens, const struct grid_param *frame, const int *nimages_strongLensing, galaxy *images, double dx, double dy, const int y_pos_loc, const int y_bound)
{
	//
	PUSH_RANGE("chi barycenter computation", 7);
	unsigned int nsets = runmode->nsets;
	//
        int numImages = 0;
        //
        int maxImagesPerSource = 0;
        for( int source_id = 0; source_id < nsets; source_id ++)
        {
                numImages += nimages_strongLensing[source_id];
                maxImagesPerSource = MAX(nimages_strongLensing[source_id], maxImagesPerSource);
        }
	MAXIMPERSOURCE = maxImagesPerSource + 2;
	struct galaxy* sources;
	cudaMallocManaged(&sources, nsets*sizeof(struct galaxy)); // theoretical sources (common for a set)
	cudaMemPrefetchAsync(sources, nsets*sizeof(struct galaxy), cudaCpuDeviceId);	
	//
	int    nimagesfound  [nsets][MAXIMPERSOURCE]; // number of images found from the theoretical sources
	int    locimagesfound[nsets];
	//int*    locimagesfound = (int*) malloc(nsets*sizeof(int));
	struct point image_pos [nsets][MAXIMPERSOURCE];
	//struct point* image_pos = (struct point*) malloc(nsets*MAXIMPERSOURCE*sizeof(struct point));	
	//double image_dist    [nsets][runmode->nimagestot][MAXIMPERSOURCE];
	struct point tim     [MAXIMPERSOURCE]; // theoretical images (computed from sources)
	int grid_size     = runmode->nbgridcells;
	int world_size = 1;
	int world_rank = 0;
#ifdef __WITH_MPI
#warning "using MPI..."
	MPI_Comm_size(MPI_COMM_WORLD, &world_size);
	MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
	MPI_Barrier(MPI_COMM_WORLD);
#endif
	unsigned int verbose = (world_rank == 0);
	//
	//const double dx   = (frame->xmax - frame->xmin)/(runmode->nbgridcells - 1);
	//const double dy   = (frame->ymax - frame->ymin)/(runmode->nbgridcells - 1);
	//
	const int grid_dim = runmode->nbgridcells;
	double time = -myseconds();
	double grad_time = -myseconds();
	PUSH_RANGE("gradient_grid_GPU_UM", 1);
	gradient_grid_GPU_UM(grid_gradient_x, grid_gradient_y, frame, lens, runmode->nhalos, dx, dy, grid_size, y_bound, 0, y_pos_loc);
//#ifdef __WITH_MPI
//	MPI_Barrier(MPI_COMM_WORLD);
//#endif
	//
	int index             = 0;       // index tracks the image within the total image array in the image plane
	*chi  		      = 0;
	//
	double chi2_time      = 0.;
	double delense_time   = 0.;
	double image_time     = 0.;
	//
	int images_found      = 0;
	long int images_total = 0;
	//
	//printf("@@nsets = %d nx = %d ny = %d, xmin = %f, dx = %f, ymin = %f, dy = %f\n", nsets, runmode->nbgridcells, runmode->nbgridcells, frame->xmin, dx, frame->ymin, dy );
	//
	//
	time = -myseconds();
	//
	// nsets     : number of images in the source plane
	// nimagestot: number of images in the image plane
	//
	//
	// Image Lensing
	//
	PUSH_RANGE("image construction", 2);
	image_time -= myseconds();
	//
	//unsigned int nsets = nsets;
	//
	for( int  source_id = 0; source_id < nsets; source_id ++)
	{
		// number of images in the image plane for the specific image (1,3,5...)
		unsigned short int nimages = nimages_strongLensing[source_id];
		//printf("@@ source_id = %d, nimages = %d\n",  source_id, nimages_strongLensing[source_id]);
		//Init sources
		sources[source_id].center.x = sources[source_id].center.y = 0.;
		//____________________________ image (constrains) loop ________________________________
		for(unsigned short int image_id = 0; image_id < nimages; image_id++)
		{
			//
			struct galaxy sources_temp;
			struct point Grad = module_potentialDerivatives_totalGradient_SOA(&images[index + image_id].center, lens, runmode->nhalos);
			//
			// find the position of the constrain in the source plane
			sources_temp.center.x = images[index + image_id].center.x - images[index + image_id].dr*Grad.x;
			sources_temp.center.y = images[index + image_id].center.y - images[index + image_id].dr*Grad.y;
			//std::cerr << "Image " << index + image_id << "x: " << images[index + image_id].center.x << " y: "<<images[index + image_id].center.y  << "Grad.x: " << Grad.x << " Grad.y: "<<Grad.y << std::endl;

			//________ Adding up for barycenter comp _________
			sources[source_id].center.x += sources_temp.center.x;
			sources[source_id].center.y += sources_temp.center.y;
			//time += myseconds();
			sources[source_id].redshift = 0.;
			sources[source_id].shape.a = sources[source_id].shape.b = sources[source_id].shape.theta = (type_t) 0.;
			sources[source_id].mag = 0.;
			//
		}
		//________ Dividing by nimages for final barycenter result _________
		sources[source_id].center.x /= nimages;
		sources[source_id].center.y /= nimages;

		sources[source_id].redshift = images[index].redshift;
		sources[source_id].dr       = images[index].dr;
		sources[source_id].dls      = images[index].dls;
		sources[source_id].dos      = images[index].dos;
		index += nimages;
	}
	cudaMemPrefetchAsync(&sources[0], MAXIMPERSOURCE*sizeof(struct galaxy), 0);
#ifdef __WITH_MPI
	MPI_Barrier(MPI_COMM_WORLD);
#endif
	image_time += myseconds();
	POP_RANGE; // image construction
	cudaDeviceSynchronize();
	grad_time += myseconds();
	POP_RANGE; // gradient computation
	//
	// Delensing
	//
	PUSH_RANGE("delense barycenter", 3);
	delense_time -= myseconds();
	index 	      = 0;
	int numimg    = 0;
	delense_barycenter_GPU(&image_pos[0][0], MAXIMPERSOURCE, &locimagesfound[0], &numimg, runmode, lens, frame, nimages_strongLensing, sources, grid_gradient_x, grid_gradient_y, y_pos_loc, y_bound);
#ifdef __WITH_MPI
	MPI_Barrier(MPI_COMM_WORLD);
#endif
	delense_time += myseconds();
	POP_RANGE;
	//
	// Communications
	//
	PUSH_RANGE("delense communication", 4);
	double comm_time = -myseconds();
	//
	int          numimagesfound    [nsets];
	struct point imagesposition    [nsets][MAXIMPERSOURCE];
	//
	memset(&numimagesfound, 0, nsets*sizeof(int));
	memset(&imagesposition, 0, nsets*MAXIMPERSOURCE*sizeof(point));
	//
	delense_comm(&numimagesfound[0], MAXIMPERSOURCE, &imagesposition[0][0],  &numimg, nimages_strongLensing, &locimagesfound[0], &image_pos[0][0], nsets, world_rank, world_size);
	//
	comm_time += myseconds();
	POP_RANGE;
	//
	// image extraction to compute
	//
	PUSH_RANGE("chi computation", 5);
	chi2_time = -myseconds();
	if (verbose)
		chi_computation(chi, &images_found, &numimagesfound[0], &imagesposition[0][0], MAXIMPERSOURCE, nimages_strongLensing, images, nsets);

#ifdef __WITH_MPI
	MPI_Barrier(MPI_COMM_WORLD);
#endif
	//
	chi2_time += myseconds();
	POP_RANGE;
	//
	// All done
	//
	time      += myseconds();
	//
	if (verbose)
	{
		//
		//		int nthreads = 1;
		//
		//#pragma omp parallel
		//		nthreads = omp_get_num_threads();
		//
		printf("	overall time  = %f s.\n", time);
		printf("		- grad    time = %f s.\n", grad_time);
		printf("		- image   time = %f s.\n", image_time);
		printf("		- delense time = %f s.\n", delense_time);
		printf("		- comm    time = %f s.\n", comm_time);
		printf("		- chi2    time = %f s.\n", chi2_time);
		//
		printf("	images found: %d out of %ld\n", images_found, images_total);
	}
	//
	PUSH_RANGE("Memory deallocation", 6);
	POP_RANGE;
	POP_RANGE;
}
