CXX=mpic++
CXXFLAGS += -fPIC -fopenmp 
CXXFLAGS += -fpermissive 
CXXFLAGS += -pedantic 
CXXFLAGS += -std=c++11
CXXFLAGS += -march=native
CXXFLAGS += -mfma
CXXFLAGS += -Ofast 
CXXFLAGS += -g

