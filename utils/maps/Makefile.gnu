PROGRAM_NAME := Bayesmap
#CXX=g++ -lm -ffast-math -ftree-loop-vectorize 
CXX=mpic++
#
program_CXX_SRCS := $(wildcard *.cpp)
$(info $$program_CXX_SRCS is [${program_CXX_SRCS}])
program_CXX_OBJS := ${program_CXX_SRCS:.cpp=.o}
$(info $$program_CXX_OBJS is [${program_CXX_OBJS}])
#
program_C_SRCS := $(wildcard *.c)
program_C_OBJS := ${program_C_SRCS:.c=.o}
#
#program_CU_SRCS := $(wildcard *.cu)
#program_CU_SRCS += $(wildcard ../../*.cu) #Find CUDA source files from additional directories
#program_CU_HEADERS := $(wildcard *.cuh) #Optional: Include .cuh files as dependencies
#program_CU_HEADERS += $(wildcard ../../*.cuh) #Find .cuh files from additional directories
#program_CU_OBJS := ${program_CU_SRCS:.cu=.cuo}
#
program_INCLUDE_DIRS := ./ 
program_INCLUDE_DIRS += $(CFITSIO_ROOT)/include
program_INCLUDE_DIRS += $(LENSTOOL_ROOT)/include
program_INCLUDE_DIRS += $(LENSTOOLHPC_ROOT)/src
program_INCLUDE_DIRS += $(GSL_ROOT)/include
program_INCLUDE_DIRS += $(MARLA_ROOT)/include
#
program_INCLUDE_LIBS := /usr/lib64/ #Include libraries
program_INCLUDE_LIBS += $(CFITSIO_ROOT)/lib #Include libraries
program_INCLUDE_LIBS += $(LENSTOOL_ROOT)/src 
program_INCLUDE_LIBS += $(WCSTOOL_ROOT)
program_INCLUDE_LIBS += $(LENSTOOLHPC_ROOT)/src 

#LDFLAGS := -llenstool -llenstoolhpc -llenstoolhpc_GPU -llt -lcfitsio -lwcs -lgsl -lgslcblas #Do not keep lenstool for the float calculation, it creates conflictinthe structure.h file
LDFLAGS := -llenstoolhpc -llenstoolhpc_GPU -llt -lcfitsio -lgsl -lgslcblas
LDFLAGS   += $(foreach includedir,$(program_INCLUDE_DIRS),-I$(includedir))
LDFLAGS   += $(foreach includelib,$(program_INCLUDE_LIBS),-L$(includelib))

# defined variables
# Compiler flags
CXXFLAGS += $(foreach includedir,$(program_INCLUDE_DIRS),-I$(includedir))
CXXFLAGS += $(foreach includelib,$(program_INCLUDE_LIBS),-L$(includelib) )
#
include ./cpuflags.gnu.inc
include $(LENSTOOLHPC_ROOT)/src/precision.inc
#
CXXFLAGS += $(PRECISION)
CXXFLAGS += -D__WITH_MPI
#CXXFLAGS += -D__WITH_MARLA
#
OBJECTS = $(program_CXX_OBJS) $(program_C_OBJS)
$(info $$OBJECTS is [${OBJECTS}])
#
.PHONY: all clean distclean
#
#all: $(PROGRAM_NAME) $(STATIC_LIB)
all: $(PROGRAM_NAME) 
#
# Rule for compilation of CUDA source (C++ source can be handled automatically)
#%.cuo: %.cu %.cuh
#	nvcc $(NVFLAGS)  -o $@ -dc $<

%.o: %.cpp %.h 
	$(CXX) $(CXXFLAGS) -c -o $@ $< 

# This is pretty ugly...details below
# The program depends on both C++ and CUDA Objects, but storing CUDA objects as .o files results in circular dependency
# warnings from Make. However, nvcc requires that object files for linking end in the .o extension, else it will throw
# an error saying that it doesn't know how to handle the file. Using a non .o rule for Make and then renaming the file 
# to have the .o extension for nvcc won't suffice because the program will depend on the non .o file but the files in
# the directory after compilation will have a .o suffix. Thus, when one goes to recompile the code all files will be
# recompiled instead of just the ones that have been updated. 
#
# The solution below solves these issues by silently converting the non .o suffix needed by make to the .o suffix 
# required by nvcc, calling nvcc, and then converting back to the non .o suffix for future, dependency-based 
# compilation.

$(PROGRAM_NAME): $(OBJECTS) 
	#$(CXX) $(CXXFLAGS)  -o $@ $(program_CXX_OBJS) $(program_C_OBJS) -llenstool 
	#$(CXX) $(CXXFLAGS)  -o $@ $(program_CXX_OBJS) $(program_C_OBJS)
	$(CXX) $(CXXFLAGS)  -o $@ $(program_CXX_OBJS) $(program_C_OBJS) $(LDFLAGS) 

$(STATIC_LIB): $(OBJECTS)
	ar rcs -o $@ $(program_CXX_OBJS) $(program_C_OBJS)
#
clean:
	@- $(RM) $(PROGRAM_NAME) $(OBJECTS) *~ *.o *.optrpt *.so *.lo

distclean: clean

