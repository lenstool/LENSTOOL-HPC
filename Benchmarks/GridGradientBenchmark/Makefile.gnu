#
PROGRAM_NAME := GridGradient_CPU
all: $(PROGRAM_NAME) 
#CXX=g++ -lm -ffast-math -ftree-loop-vectorize 
#CXX=icpc
#
program_CXX_SRCS := $(wildcard *.cpp)
program_CXX_OBJS := ${program_CXX_SRCS:.cpp=.o}
#
program_C_SRCS := $(wildcard *.c)
program_C_OBJS := ${program_C_SRCS:.c=.o}
#
program_CU_SRCS := $(wildcard *.cu)
program_CU_OBJS := ${program_CU_SRCS:.cu=.cu.o}
#
# include
#
program_INCLUDE_DIRS := . /usr/local/cuda/include/ #C++ Include directories
program_INCLUDE_DIRS += $(CFITSIO_ROOT)/include
program_INCLUDE_DIRS += $(LENSTOOL_ROOT)/include
program_INCLUDE_DIRS += $(GSL_ROOT)/include
program_INCLUDE_DIRS += $(LENSTOOLHPC_ROOT)/src
#
# libs
#
program_INCLUDE_LIBS += $(CFITSIO_ROOT)/lib #Include libraries
program_INCLUDE_LIBS += $(LENSTOOL_ROOT)/src
program_INCLUDE_LIBS += $(LENSTOOL_ROOT)/liblt
program_INCLUDE_LIBS += $(LENSTOOLHPC_ROOT)/src
program_INCLUDE_LIBS += $(GSL_ROOT)/lib
program_INCLUDE_LIBS += $(WCSTOOL_ROOT)
#
#
# Compiler flags
CXXFLAGS += $(foreach includedir,$(program_INCLUDE_DIRS),-I$(includedir))
CXXFLAGS += $(foreach includelib,$(program_INCLUDE_LIBS),-L$(includelib))
#CXXFLAGS += -D__WITH_LENSTOOL 
CXXFLAGS += -D__WITH_MPI
include $(LENSTOOLHPC_ROOT)/src/cpuflags.gnu.inc
include $(LENSTOOLHPC_ROOT)/src/precision.inc
CXXFLAGS += $(PRECISION)

#CXXFLAGS += -no-vec
#CXXFLAGS += -qopenmp -march=core-avx2 -g -O3 -std=c++0x -Wall -pedantic
#CXXFLAGS += -llenstoolhpc -qopenmp -xHost -g -O3 -std=c++0x -Wall -pedantic
#CXXFLAGS += -llenstoolhpc -qopenmp -axMIC-AVX512,CORE-AVX2 -g -O3 -std=c++0x -Wall -pedantic
#CXXFLAGS += -qopt-prefetch-distance=64,8 -qopt-streaming-cache-evict=0 -llenstoolhpc -qopenmp -xMIC-AVX512 -g -O3 -std=c++0x -Wall -pedantic
LDFLAGS := -llenstool -llenstoolhpc -llt -lcfitsio -lwcs -lgsl -lgslcblas -lgomp
#
LDFLAGS   += $(foreach includedir,$(program_INCLUDE_DIRS),-I$(includedir))
LDFLAGS   += $(foreach includelib,$(program_INCLUDE_LIBS),-L$(includelib))
#
LDFLAGS   += -O3 -g 
#
#
CPU_OBJECTS = $(program_CXX_OBJS) $(program_C_OBJS)
#
%.cpp: %.cpp %.h
	$(CXX) $(CXXFLAGS) -o $@ $< $(CXXFLAGS)
#
$(PROGRAM_NAME): $(CPU_OBJECTS) 
	$(CXX) -o $@ $^ $(LDFLAGS) 

clean:
	@- $(RM) $(PROGRAM_NAME) $(OBJECTS) *~ *.o *.optrpt

distclean: clean

.PHONY: all clean distclean
