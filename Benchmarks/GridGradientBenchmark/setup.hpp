#include <iostream>
#include <string.h>
//#include <cuda_runtime.h>
#include <math.h>
#include <sys/time.h>
#include <fstream>

//#include "simd_math.h"

#ifdef __WITH_LENSTOOL
#include "structure.h"

extern struct pot lens[NLMAX];

void
//setup_jauzac_LT(struct pot** lens, int* nlenses, double* x, double* y, double* sol_grad_x, double* sol_grad_y)
convert_to_LT(Potential_SOA* lenses, int nlenses)
{
        //
        //*lens = (struct pot*) malloc(sizeof(struct pot)*(*nlenses));
        //
        for (int i = 0; i < nlenses; ++i)
        {
                //
                lens[i].C.x            	      = lenses->position_x[i];
                lens[i].C.y                   = lenses->position_y[i];
                //
                lens[i].sigma                 = 1.; 			// disp
                if (lenses->type[i] == 5) {
                	lens[i].type = 1;
                }
                else
		{
                	lens[i].type = lenses->type[i];
                }
                lens[i].emass                 = 0.11;
                lens[i].epot                  = lenses->ellipticity_potential[i];
		//printf("%f %f\n", lenses->ellipticity_potential[i], lenses->ellipticity[i]);
		//printf("%f %f\n", lens[i].epot, lenses->ellipticity_potential[i]);
                //lens[i].epot                  = lenses->ellipticity[i];
                lens[i].theta                 = lenses->ellipticity_angle[i];
		lens[i].costheta	      = cos(lens[i].theta);
		lens[i].sintheta	      = sin(lens[i].theta);
                lens[i].rcut                  = lenses->rcut[i];
                lens[i].rc                    = lenses->rcore[i];
                lens[i].b0                    = lenses->b0[i];
		lens[i].z		      = lenses->z[i];
                lens[i].masse                 = 0;			// weight
                //lens[i].rc  	                 = 0;			// rscale
//               (&lens)[i].exponent              = 0;
                lens[i].alpha                 = 0.;
//                (&lens)[i].einasto_kappacritic   = 0;
                //lens[i].z                     = 0.4;
        }
}
#endif
#include "structure_hpc.hpp"

void
convert_SOA_to_AOS(Potential* lenses_AOS, const Potential_SOA* lenses, int nlenses)
{
        //
        //*lens = (struct pot*) malloc(sizeof(struct pot)*(*nlenses));
        //
        for (int i = 0; i < nlenses; ++i)
        {
                //
                lenses_AOS[i].position.x            = lenses->position_x[i];
                lenses_AOS[i].position.y            = lenses->position_y[i];
                //
                lenses_AOS[i].sigma                 = lenses->sigma[i];                     // disp
		lenses_AOS[i].type 		    = lenses->type[i];
                //lenses_AOS[i].emass                 = lenses->emass[i];
                lenses_AOS[i].ellipticity_potential = lenses->ellipticity_potential[i];
                //printf("%f %f\n", lenses->ellipticity_potential[i], lenses->ellipticity[i]);
                //printf("%f %f\n", lens[i].epot, lenses->ellipticity_potential[i]);
                //lens[i].epot                  = lenses->ellipticity[i];
                lenses_AOS[i].ellipticity_angle     = lenses->ellipticity_angle[i];
		lenses_AOS[i].theta		    = lenses->ellipticity_angle[i];
		//printf("AOS: %f %f\n", lenses->ellipticity_angle[i], lenses_AOS[i].ellipticity_angle);
                lenses_AOS[i].anglecos              = cos(lenses->ellipticity_angle[i]);
                lenses_AOS[i].anglesin              = sin(lenses->ellipticity_angle[i]);
                lenses_AOS[i].rcut                  = lenses->rcut[i];
                lenses_AOS[i].rcore                 = lenses->rcore[i];
                lenses_AOS[i].b0                    = lenses->b0[i];
                lenses_AOS[i].z                     = lenses->z[i];
                //lenses_AOS[i].masse                 = 0;                      // weight
                //lens[i].rc                     = 0;                   // rscale
//               (&lens)[i].exponent              = 0;
                lenses_AOS[i].alpha                 = 0.;
//                (&lens)[i].einasto_kappacritic   = 0;
                //lens[i].z                     = 0.4;
        }
}













