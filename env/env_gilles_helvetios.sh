#echo "\$BASH_SOURCE ${BASH_SOURCE[@]}"
#echo "$PWD/${BASH_SOURCE[@]}"
#echo dirname "$PWD/${BASH_SOURCE[@]}"
#echo $(realpath "$(dirname "$PWD/${BASH_SOURCE[@]}")")
MYPATH=$(realpath "$(dirname "$PWD/../${BASH_SOURCE[@]}")")
#echo $MYPATH
export LENSTOOL_ROOT=/work/scitas-share/foureste/Lenstool/lenstool-7.0-avx512/
#export LENSTOOL_ROOT=/work/scitas-share/foureste/Lenstool/lenstool-6.8.1/
#export LENSTOOLHPC_ROOT=/users/fgilles/GPU-Projects/lenstool-hpc-master/
export LENSTOOLHPC_ROOT=$MYPATH
export LIBS=/work/scitas-share/foureste/Lenstool
export CFITSIO_ROOT=$LIBS/cfitsio/
export WCSTOOL_ROOT=$LIBS/wcstools-3.8.4/libwcs/
export GSL_ROOT=$LIBS/gsl-2.2/
export MARLA_ROOT=/users/fgilles/scratch/Projects/marla/
#
export LD_LIBRARY_PATH=$LENSTOOLHPC_ROOT/src:$GSL_ROOT/lib:$LD_LIBRARY_PATH 
#export PATH=/cm/shared/apps/intel-2017/vtune_amplifier_xe/bin64/:$PATH
#
module load intel intel-mpi gsl
export CUDA_ROOT=/usr/local/cuda/
export PATH=$CUDA_ROOT/bin:$PATH
export LD_LIBRARY_PATH=$CUDA_ROOT/lib64:$LD_LIBRARY_PATH
#
#module load cuda92
#
export CXXFLAGS=" -Ofast -xCORE-AVX512 -qopt-zmm-usage=high "
export CXX=mpiicpc
#export CXX=mpicxx
