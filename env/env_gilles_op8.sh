#echo "\$BASH_SOURCE ${BASH_SOURCE[@]}"
#echo "$PWD/${BASH_SOURCE[@]}"
#echo dirname "$PWD/${BASH_SOURCE[@]}"
#echo $(realpath "$(dirname "$PWD/${BASH_SOURCE[@]}")")
MYPATH=$(realpath "$(dirname "$PWD/../${BASH_SOURCE[@]}")")
#echo $MYPATH
export LENSTOOL_ROOT=/users/fgilles/Projects/Lenstool/lenstool-6.8.1-OP8/
export LENSTOOLHPC_ROOT=$MYPATH
export CFITSIO_ROOT=/users/fgilles/Projects/Lenstool/cfitsio-OP8/
export WCSTOOL_ROOT=/users/fgilles/Projects/Lenstool/wcstools-3.9.4-OP8/
export GSL_ROOT=/users/fgilles/Projects/Lenstool/gsl-2.2-OP8/
#
export LD_LIBRARY_PATH=$LENSTOOLHPC_ROOT/src:$GSL_ROOT/lib:$LD_LIBRARY_PATH 
#export PATH=/cm/shared/apps/intel-2017/vtune_amplifier_xe/bin64/:$PATH
#
#module load intel/compiler intel/mpi intel/mkl
export CUDA_ROOT=/usr/local/cuda/

module load GCC/5.4.0
module load cuda/8.0.54

export PATH=$CUDA_ROOT/bin:$PATH
export LD_LIBRARY_PATH=$CUDA_ROOT/lib64:$LD_LIBRARY_PATH

export PATH=$HOME/Projects/openmpi-4.0.1rc1/bin:$PATH
export LD_LIBRARY_PATH=$HOME/Projects/openmpi-4.0.1rc1/lib:$LD_LIBRARY_PATH
#
#module load cuda92
#
export CXX=mpic++
