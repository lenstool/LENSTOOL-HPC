#!/bin/bash

export LENSTOOL_ROOT=/home/christoph/Documents/Workplace/lenstool-6.8.1/lenstool-6.8.1
export LENSTOOLHPC_ROOT=/home/christoph/Documents/Workplace/Lenstool-HPC/lenstool-hpc
export CFITSIO_ROOT=/home/christoph/Documents/Workplace/lenstool-6.8.1/Libs/cfitsio3280/cfitsio
export WCSTOOL_ROOT=/home/christoph/Documents/Workplace/lenstool-6.8.1/Libs/wcstools-3.9.5/wcstools-3.9.5/libwcs
export GSL_ROOT=/home/christoph/Documents/Workplace/lenstool-6.8.1/Libs/gsl-latest/gsl-2.3

export LD_LIBRARY_PATH=/home/christoph/Documents/Workplace/Lenstool-HPC/lenstool-hpc/src:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=/home/christoph/Documents/Workplace/lenstool-6.8.1/Libs/gsl-latest/gsl-2.3/lib:$LD_LIBRARY_PATH

#export CXX=/usr/bin/gcc
#export CXX=/usr/bin/mpicc
#export CXX=/usr/bin/g++
export CXX=/usr/bin/mpic++